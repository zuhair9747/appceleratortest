package com.appcel.zuhair.appceltest.adapter.holder;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appcel.zuhair.appceltest.R;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ARelativeHumidity;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ATemperature;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AWind;
import com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast.AForecast;
import com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast.AForecastSlot;
import com.appcel.zuhair.appceltest.common.utility.Utils;

/**
 * Created by zuhair on 24/9/16.
 */

public class DayForecastHolder extends ViewHolder{

    AForecast forecast;

    TextView tempTxt;
    TextView humidTxt;
    TextView windTxt;

    Button nightBtn;
    Button morningBtn;
    Button afternoonBtn;

    ImageView wxnorth_txt,wxwest_txt,wxcentral_txt,wxeast_txt,wxsouth_txt;
    TextView wxnorth_txt2,wxwest_txt2,wxcentral_txt2,wxeast_txt2,wxsouth_txt2;

    public DayForecastHolder(){

    }

    public DayForecastHolder(AForecast forecast){
        this.forecast = forecast;
    }


    @Override
    public void renderView(View v) {

        tempTxt         = (TextView)v.findViewById(R.id.temp_txt);
        humidTxt        = (TextView)v.findViewById(R.id.humid_txt);
        windTxt         = (TextView)v.findViewById(R.id.wind_txt);

        nightBtn        = (Button)v.findViewById(R.id.night_btn);
        morningBtn      = (Button)v.findViewById(R.id.morning_btn);
        afternoonBtn    = (Button)v.findViewById(R.id.afternoon_btn);

        wxnorth_txt     = (ImageView)v.findViewById(R.id.wxnorth_txt);
        wxwest_txt      = (ImageView)v.findViewById(R.id.wxwest_txt);
        wxcentral_txt   = (ImageView)v.findViewById(R.id.wxcentral_txt);
        wxeast_txt      = (ImageView)v.findViewById(R.id.wxeast_txt);
        wxsouth_txt     = (ImageView)v.findViewById(R.id.wxsouth_txt);

        wxnorth_txt2     = (TextView)v.findViewById(R.id.wxnorth_txt2);
        wxwest_txt2      = (TextView)v.findViewById(R.id.wxwest_txt2);
        wxcentral_txt2   = (TextView)v.findViewById(R.id.wxcentral_txt2);
        wxeast_txt2      = (TextView)v.findViewById(R.id.wxeast_txt2);
        wxsouth_txt2     = (TextView)v.findViewById(R.id.wxsouth_txt2);

        initializeListener();
        updateDataInfo();

    }

    @Override
    public int getLayout() {
        return R.layout.day_forecast_cview;
    }

    @Override
    protected void updateDataInfo() {

        ATemperature temperature = forecast.channel._main.temperature;
        ARelativeHumidity relativeHumidity = forecast.channel._main.relativeHumidity;
        AWind wind = forecast.channel._main.wind;

        tempTxt.setText(temperature.low+" - "+temperature.high+" "+temperature.getUnit());
        humidTxt.setText(relativeHumidity.low + " - "+relativeHumidity.high+" "+relativeHumidity.getUnit());
        windTxt.setText(wind.speed);

        AForecastSlot night = forecast.channel.night;
        setIcons(night);


    }

    void initializeListener(){

        nightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AForecastSlot night = forecast.channel.night;
                setIcons(night);
            }
        });

        morningBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AForecastSlot morning = forecast.channel.morning;
                setIcons(morning);
            }
        });

        afternoonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AForecastSlot afternoon = forecast.channel.afternoon;
                setIcons(afternoon);
            }
        });
    }


    void setIcons(AForecastSlot slot){

        wxnorth_txt.setBackgroundResource(Utils.getIconFromAbbreviation(slot.wxnorth));
        wxwest_txt.setBackgroundResource(Utils.getIconFromAbbreviation(slot.wxwest));
        wxcentral_txt.setBackgroundResource(Utils.getIconFromAbbreviation(slot.wxcentral));
        wxeast_txt.setBackgroundResource(Utils.getIconFromAbbreviation(slot.wxeast));
        wxsouth_txt.setBackgroundResource(Utils.getIconFromAbbreviation(slot.wxsouth));

        wxnorth_txt2.setText(Utils.getTextFromAbbreviation(slot.wxnorth));
        wxwest_txt2.setText(Utils.getTextFromAbbreviation(slot.wxwest));
        wxcentral_txt2.setText(Utils.getTextFromAbbreviation(slot.wxcentral));
        wxeast_txt2.setText(Utils.getTextFromAbbreviation(slot.wxeast));
        wxsouth_txt2.setText(Utils.getTextFromAbbreviation(slot.wxsouth));

    }


}
