package com.appcel.zuhair.appceltest.common.model.responsemodel;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.AArea;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AWeatherForecast extends JsonObj {

    public ArrayList<AArea> areaList;



    public AWeatherForecast(JSONObject map) {
        super(map);
        if(!isEmpty){

            JSONArray areayArr = AJSONObject.optJSONArray(map, "area");
            areaList			= new ArrayList<AArea>();
            if(areayArr!=null){
                for (int i = 0; i < areayArr.length(); i++) {
                    areaList.add(new AArea(areayArr.optJSONObject(i)));
                }
            }



        }
    }
}
