package com.appcel.zuhair.appceltest.custom;

import java.util.ArrayList;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.appcel.zuhair.appceltest.common.utility.Log;


//====================================================================
public abstract class HeaderListAdapter extends BaseAdapter{

	protected ArrayList<Object> items = new ArrayList<Object>();
	private int HeaderView = 1, ItemView = 2;
	private Header lastKnownHeader = null;
	
	//=============================================
	public int getCount() {
		return items.size();
	}
	
	//=============================================
	public ArrayList<Object> getAll(){return items;}
	
	//=============================================
	public void clear(){ items.clear(); }
	
	//=============================================
	@Override
	public int getViewTypeCount(){return 3;/* With 2 it was crashing, don't know why */}

	//=============================================
	public Object getItem(int arg0) {
		Object o = items.get(arg0);
		if(o instanceof Header)
			return ((Header)o).object;
		else
			return ((Item)o).object;
	}

	//=============================================
	@Override
	public int getItemViewType(int pos){
		
		Object item = null;
		try{
			item = items.get(pos);
		}
		catch(IndexOutOfBoundsException e){
			e.printStackTrace();
			Log.e(e.getMessage()+" At getItemViewType(...) of HeaderListAdapter");
		}
		
		if(item!=null && item instanceof Header)
			return HeaderView;
		else 
			return ItemView;
		
	}
	
	//=============================================
	public long getItemId(int arg0) {return arg0;}

	//=============================================
	public View getView(int pos, View arg1, ViewGroup arg2) {
		
		Object item = items.get(pos);
		if(item instanceof Header)
			return getHeaderView(pos, arg1, arg2, ((Header)item).object, ((Header)item).itemCount);
		else
			return getItemView(pos, arg1, arg2, ((Item)item).object);
	}
	
	//=============================================
	public ArrayList<Header> getHeaders(){
		return getFilteredItems(Header.class);
	}
	
	public ArrayList<Item> getItems(){
		return getFilteredItems(Item.class);
	}
	
	@SuppressWarnings("rawtypes")
	private ArrayList getFilteredItems(Class type){
		
		ArrayList<Object> filteredData = new ArrayList<Object>();
		
		for (Object object : items) {
			if(object.getClass().equals(type)){
				filteredData.add(object);
			}
		}
		
		return filteredData;
		
	}


	//=============================================
	public Header addHeader(Object o){
		Header h = new Header(o);
		items.add(h);
		lastKnownHeader = h;
		return h;
	}
	
	//=============================================
	public Item addItem(Object o){
		Item i = new Item(o);
		items.add(i);
		//==update Header count if exist
		if(lastKnownHeader!=null)
			lastKnownHeader.itemCount++;
		return i;
	}
	
	//=============================================
	/*public void addAtpos(int pos, Object o){
		items.add(pos, o);
	}*/
	
	//=============================================
	public abstract View getHeaderView(int pos, View v, ViewGroup vg, Object o, int itemCount);
	
	//=============================================
	public abstract View getItemView(int pos, View v, ViewGroup vg, Object o);

	
	
	//====================================================================
	public class Header{
		
		public Object object;
		public int itemCount; 
		
		//=============================================
		public Header(Object o){
			this.object = o;
			this.itemCount = 0;
		}

	}
	
	//====================================================================
	public class Item{
		
		public Object object;
		
		//=============================================
		public Item(Object o){this.object=o;}
	}
}
