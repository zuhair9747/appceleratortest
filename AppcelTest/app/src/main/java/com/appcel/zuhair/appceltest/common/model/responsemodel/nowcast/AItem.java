package com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AForecastIssue;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AWeatherForecast;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AItem extends JsonObj {

    public String title;
    public String category;
    public String validTime;
    public AForecastIssue forecastIssue;
    public AWeatherForecast weatherForecast;

    public AItem(JSONObject map) {
        super(map);
        if(!isEmpty){

            title 		    = AJSONObject.optString(map, "title");
            category 	    = AJSONObject.optString(map, "category");
            validTime       = AJSONObject.optString(map, "validTime");
            forecastIssue   = new AForecastIssue(AJSONObject.optJSONObject(map, "forecastIssue"));
            weatherForecast = new AWeatherForecast(AJSONObject.optJSONObject(map, "weatherForecast"));


        }
    }

}
