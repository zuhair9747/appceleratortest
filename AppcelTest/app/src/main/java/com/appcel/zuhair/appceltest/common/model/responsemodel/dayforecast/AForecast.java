package com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AForecast extends JsonObj {


    public AChannelDF channel;

    public AForecast(JSONObject map) {
        super(map);
        if(!isEmpty){

            channel = new AChannelDF(AJSONObject.optJSONObject(map, "channel"));

        }
    }
}
