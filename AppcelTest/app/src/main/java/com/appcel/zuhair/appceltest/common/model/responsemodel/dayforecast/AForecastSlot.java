package com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 22/9/16.
 */
public class AForecastSlot extends JsonObj {

    public String timePeriod;
    public String wxeast;
    public String wxwest;
    public String wxnorth;
    public String wxsouth;
    public String wxcentral;


    public AForecastSlot(JSONObject map) {
        super(map);
        if(!isEmpty){

            timePeriod  = AJSONObject.optString(map, "timePeriod");
            wxeast 	    = AJSONObject.optString(map, "wxeast");
            wxwest 		= AJSONObject.optString(map, "wxwest");
            wxnorth     = AJSONObject.optString(map, "wxnorth");
            wxsouth 	= AJSONObject.optString(map, "wxsouth");
            wxcentral 	= AJSONObject.optString(map, "wxcentral");


        }
    }


}
