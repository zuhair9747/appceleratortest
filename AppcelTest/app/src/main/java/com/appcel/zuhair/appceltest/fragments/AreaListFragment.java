package com.appcel.zuhair.appceltest.fragments;


import android.app.DialogFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import com.appcel.zuhair.appceltest.R;
import com.appcel.zuhair.appceltest.adapter.AreaListAdapter;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.AArea;
import com.appcel.zuhair.appceltest.common.utility.Log;

import java.util.ArrayList;

/**
 * Created by zuhair on 25/9/16.
 */

public abstract class AreaListFragment extends DialogFragment{

    private RecyclerView.LayoutManager mLayoutManager;

    RecyclerView areaRecyclerView;

    AreaListAdapter adapter;
    ImageButton back;

    ArrayList<AArea> areaList;

    public  AreaListFragment(){

    }



    public  AreaListFragment(ArrayList<AArea> areaList){
        this.areaList = areaList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.AppTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.area_list, container, false);

        if (savedInstanceState ==  null) {

            areaRecyclerView    = (RecyclerView)rootView.findViewById(R.id.area_recycler_view);
            back                = (ImageButton)rootView.findViewById(R.id.back);


            mLayoutManager	= new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
            areaRecyclerView.setLayoutManager(mLayoutManager);

            initializeListener();
            initializeAdapter();

        }

        return rootView;
    }

    void initializeAdapter(){

        adapter = new AreaListAdapter(getActivity(),areaList);

        areaRecyclerView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

    }

    void initializeListener(){

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public abstract void onBackPressed();
}
