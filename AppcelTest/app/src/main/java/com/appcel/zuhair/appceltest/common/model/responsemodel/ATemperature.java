package com.appcel.zuhair.appceltest.common.model.responsemodel;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class ATemperature extends JsonObj {

    public String high;
    public String low;
    public String unit;


    public ATemperature(JSONObject map) {
        super(map);
        if(!isEmpty){

            high    = AJSONObject.optString(map, "high");
            low     = AJSONObject.optString(map, "low");
            unit    = AJSONObject.optString(map, "unit");


        }
    }

    public String getUnit(){
        if (unit.equalsIgnoreCase("Degrees Celsius")){
            return "°C";
        }else if (unit.equalsIgnoreCase("Fahrenheit")){
            return "°F";
        }

        return "";
    }
}

