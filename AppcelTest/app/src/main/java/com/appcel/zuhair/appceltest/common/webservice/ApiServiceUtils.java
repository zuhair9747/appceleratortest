package com.appcel.zuhair.appceltest.common.webservice;

import android.content.Context;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.utility.Utils;
import com.appcel.zuhair.appceltest.common.webservice.ApiServiceBase.ServerUrlAction;
import com.appcel.zuhair.appceltest.common.utility.Log;
import com.appcel.zuhair.appceltest.common.webservice.preferences.ApiServicePreferences;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;

public class ApiServiceUtils {


	public static String getUrl(Context ctx){
        return getBaseUrl(ctx);
    }
	
    public static String getUrl(Context ctx, JSONObject params){
        return addQueryToUrl(getBaseUrl(ctx), params);
    }
    
    public static String getUrl(Context ctx, int api){
        return getCompleteApiUrl(ctx, api, ServerUrlAction.None, null);
    }
    
    public static String getUrl(Context ctx, int api, JSONObject params){
        return getCompleteApiUrl(ctx, api, ServerUrlAction.None, params);
    }
   
    //======================================================================
    
    private static String getCompleteApiUrl(Context ctx, int api, ServerUrlAction action, JSONObject params){
        
    	String url = getBaseUrl(ctx) + getApiUrl(ctx, api);
    	
    	/*
    	 * Validate the server url action and add it to the url if there is any specific action.
    	 */
    	
    	if(action==ServerUrlAction.None){
    		//TODO nothing here as no specific action found.
    	}
    	else{
    		url += "/"+action.toString();
    	}
    	
    	/*
    	 * Validate the url params and add if any param exist then add it to the url.
    	 */
    	
    	url = addQueryToUrl(url, params);
    	
    	return url;
    	
    }
    
    private static String getBaseUrl(Context ctx){
    	//return ApiServicePreferences.getServerProtocol(ctx)+"://"+ApiServicePreferences.getServerUrl(ctx);
		return "http://"+ApiServicePreferences.getServerUrl(ctx);
    }
    
    
    private static String getApiUrl(Context ctx, int api){
    	
    	String apiStr = "v1";
    	Log.e((apiStr.isEmpty())?apiStr:"/api/"+apiStr+"/"+ctx.getString(api));
    	//return (apiStr.isEmpty())?apiStr:"/api/"+apiStr+"/"+ctx.getString(api);
		return "/api/"+ctx.getString(api);
    }
    

    //======================================================================
    
    public static String addQueryToUrl(String url, JSONObject params){
    	
    	if(params!=null && params.length()>0){
    		for(Iterator<String> iter = params.keys(); iter.hasNext();) {
			    String key = iter.next();
			    try{
				    String encodedQuery = key+"="+ URLEncoder.encode(AJSONObject.optString(params, key,""),"UTF-8");
	    			url += ((url.contains("?")?"&":"?")+encodedQuery);
			    }
			    catch (UnsupportedEncodingException e) {
		    		Log.e(ApiServiceBase.TAG, e.getMessage()+" At addQueryToUrl(...) of ApiServiceUtils");
					e.printStackTrace();
				}

			}
    	}
    	
    	return url;
    	
    }
    

    
    
    //======================================================================
    
    public static boolean callService(Context ctx, Runnable r){
    	return callService(ctx, r, true);
    }
    
	public static boolean callService(Context ctx, Runnable r, boolean showAlert){
        
		boolean status = true;
		
		if(Utils.isNetworkConnected(ctx, showAlert)){
			Thread t = new Thread(r);
	        t.setName(r.getClass().getName());
	        t.start();
        }
        else{
        	status = false;
        }    
		
		return status;
		
	}

}
