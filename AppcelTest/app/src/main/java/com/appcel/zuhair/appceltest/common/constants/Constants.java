package com.appcel.zuhair.appceltest.common.constants;

/**
 * Created by zuhairahmad on 20/9/16.
 */
public class Constants {

    public static final String kNoneTypeError	=	"ErrorType Not Found!!!";
    public static final int kNone				=	-1;
    public static final int kZero				=	 0;

    public static final String kApiCA			=	"api_ca";
    public static final String kApiUrl			=	"api_url";
}
