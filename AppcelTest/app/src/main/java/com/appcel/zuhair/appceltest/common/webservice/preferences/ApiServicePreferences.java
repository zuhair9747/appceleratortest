package com.appcel.zuhair.appceltest.common.webservice.preferences;

import android.content.Context;

import com.appcel.zuhair.appceltest.R;

public class ApiServicePreferences {

	public enum ServerUrlProtocolType{
		http, https
	}

    public static String getServerUrl(Context ctx){
    	return getServerUrl(ctx, true);
    }

    public static boolean isDefaultServer(Context ctx){
        return getServerUrl(ctx, true).equals(getDefaultUrl(ctx));
    }
    
    public static String getServerUrl(Context ctx, boolean returnDefault){

		String serverUrl = getDefaultUrl(ctx);

    	return serverUrl;
    }

    private static String getDefaultUrl(Context ctx){
    	return ctx.getString(R.string.api_server);
    }

    private static boolean validateUrl(String url, String defaultUrl){
    	return url == null || url.isEmpty() || url.equalsIgnoreCase(defaultUrl);
    }

}
