package com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AChannelDF extends JsonObj {

    public String title;
    public String source;
    public AMain _main;
    public AForecastSlot night;
    public AForecastSlot morning;
    public AForecastSlot afternoon;


    public AChannelDF(JSONObject map) {
        super(map);
        if(!isEmpty){

            title 		= AJSONObject.optString(map, "title");
            source 		= AJSONObject.optString(map, "source");
            _main       = new AMain(AJSONObject.optJSONObject(map, "main"));
            night       = new AForecastSlot(AJSONObject.optJSONObject(map, "night"));
            morning     = new AForecastSlot(AJSONObject.optJSONObject(map, "morn"));
            afternoon   = new AForecastSlot(AJSONObject.optJSONObject(map, "afternoon"));



        }
    }


}