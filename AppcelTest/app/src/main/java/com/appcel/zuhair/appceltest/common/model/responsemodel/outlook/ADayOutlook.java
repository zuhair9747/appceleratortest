package com.appcel.zuhair.appceltest.common.model.responsemodel.outlook;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ARelativeHumidity;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ATemperature;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.AChannelNF;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class ADayOutlook extends JsonObj {

    public AChannelDO channel;



    public ADayOutlook(JSONObject map) {
        super(map);
        if(!isEmpty){

            channel = new AChannelDO(AJSONObject.optJSONObject(map, "channel"));


        }
    }
}
