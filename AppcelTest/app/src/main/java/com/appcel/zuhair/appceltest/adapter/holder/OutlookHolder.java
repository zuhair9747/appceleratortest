package com.appcel.zuhair.appceltest.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.appcel.zuhair.appceltest.R;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ARelativeHumidity;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ATemperature;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AWind;
import com.appcel.zuhair.appceltest.common.utility.Utils;

import java.util.ArrayList;

/**
 * Created by zuhair on 24/9/16.
 */

public class OutlookHolder extends ViewHolder{

    String _day;
    String forecast;
    String icon;
    ATemperature temperature;
    ARelativeHumidity relativeHumidity;
    AWind wind;

    TextView forecastTxt;
    TextView tempTxt;
    TextView windTxt;

    TextView dayTxt;

    ImageView forecast_icon;

    public OutlookHolder(String _day,String forecast,String icon,ATemperature temperature,ARelativeHumidity relativeHumidity,AWind wind){

        this._day               = _day;
        this.forecast           = forecast;
        this.icon               = icon;
        this.temperature        = temperature;
        this.relativeHumidity   = relativeHumidity;
        this.wind               = wind;
    }

    @Override
    public void renderView(View v) {

        forecastTxt = (TextView)v.findViewById(R.id.forecast_txt);
        tempTxt     = (TextView)v.findViewById(R.id.temp_txt);
        windTxt     = (TextView)v.findViewById(R.id.wind_txt);
        dayTxt      = (TextView)v.findViewById(R.id.day_txt);

        forecast_icon = (ImageView)v.findViewById(R.id.forecast_icon);

        updateDataInfo();

    }

    @Override
    public int getLayout() {
        return R.layout.outlook_cview;
    }

    @Override
    protected void updateDataInfo() {
        forecastTxt.setText(forecast);
        tempTxt.setText(temperature.low+" - "+temperature.high+" "+temperature.getUnit());
        windTxt.setText(wind.speed);
        dayTxt.setText(_day);
        forecast_icon.setBackgroundResource(Utils.getIconFromAbbreviation(icon));

    }


}
