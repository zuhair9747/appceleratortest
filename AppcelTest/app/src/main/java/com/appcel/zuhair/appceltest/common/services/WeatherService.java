package com.appcel.zuhair.appceltest.common.services;

import android.content.Context;

import com.appcel.zuhair.appceltest.R;
import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JResponseError;
import com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast.AForecast;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.ANowcast;
import com.appcel.zuhair.appceltest.common.model.responsemodel.outlook.ADayOutlook;
import com.appcel.zuhair.appceltest.common.utility.Log;
import com.appcel.zuhair.appceltest.common.utility.Utils;
import com.appcel.zuhair.appceltest.common.webservice.ApiServiceBase.ServerReuqestMethod;
import com.appcel.zuhair.appceltest.common.webservice.ApiServiceUtils;
import com.appcel.zuhair.appceltest.common.webservice.api.RUServiceBase;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zuhairahmad on 20/9/16.
 */
public class WeatherService {

    String DATASET = "dataset";
    String KEY_REF = "keyref";


    public interface NowcastListeners {
        void onSuccess(ANowcast nocast);
        void onFailure(Exception error);

    }

    public interface DayForecastListeners{
        void onSuccess(AForecast forecast);
        void onFailure(Exception error);

    }

    public interface OutlookListeners{
        void onSuccess(ADayOutlook dayOutlook);
        void onFailure(Exception error);

    }

    private Context context;

    public WeatherService(Context context){
        this.context = context;
    }

    public boolean getTwoHourNowcast(final NowcastListeners listeners){


        JSONObject params = new JSONObject();
        try {
            params.put(DATASET, context.getString(R.string.two_hr_nowcast));
            params.put(KEY_REF, context.getString(R.string.keyref));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(e.getMessage()+" at getTwoHourNowcast Module!!");
        }

        RUServiceBase service = new RUServiceBase(context, R.string.weather_api, params, null,  null, ServerReuqestMethod.GET){

            @Override
            protected boolean onResponseReceived(String response,JSONObject JSON) throws JResponseError {
                boolean status = super.onResponseReceived(response, JSON);

                if(JSON.isNull("error")){
                    if(listeners!=null){

                        printJson(JSON);
                        listeners.onSuccess(new ANowcast(JSON));
                    }
                }
                else{
                    throw new JResponseError(Utils.getMapFromJsonObject(AJSONObject.optJSONObject(JSON, "error")));
                }

                return status;

            }

            @Override
            protected void onError(Exception e) {
                super.onError(e);
                if(listeners!=null){
                    listeners.onFailure(e);
                }
            }


        };

        return ApiServiceUtils.callService(context, service);

    }

    public boolean getForecastForDay(final DayForecastListeners listeners){

        JSONObject params = new JSONObject();
        try {
            params.put(DATASET, context.getString(R.string.day_forecast));
            params.put(KEY_REF, context.getString(R.string.keyref));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(e.getMessage()+" at getTwoHourNowcast Module!!");
        }

        RUServiceBase service = new RUServiceBase(context, R.string.weather_api, params, null,  null, ServerReuqestMethod.GET){

            @Override
            protected boolean onResponseReceived(String response,JSONObject JSON) throws JResponseError {
                boolean status = super.onResponseReceived(response, JSON);

                if(JSON.isNull("error")){
                    if(listeners!=null){
                        printJson(JSON);
                        listeners.onSuccess(new AForecast(JSON));
                    }
                }
                else{
                    throw new JResponseError(Utils.getMapFromJsonObject(AJSONObject.optJSONObject(JSON, "error")));
                }

                return status;

            }

            @Override
            protected void onError(Exception e) {
                super.onError(e);
                if(listeners!=null){
                    listeners.onFailure(e);
                }
            }

        };

        return ApiServiceUtils.callService(context,service);

    }


    public boolean getFourDayOutlook(final OutlookListeners listeners){

        JSONObject params = new JSONObject();
        try {
            params.put(DATASET, context.getString(R.string.four_days_outlook));
            params.put(KEY_REF, context.getString(R.string.keyref));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(e.getMessage()+" at getTwoHourNowcast Module!!");
        }

        RUServiceBase service = new RUServiceBase(context, R.string.weather_api, params, null,  null, ServerReuqestMethod.GET){

            @Override
            protected boolean onResponseReceived(String response,JSONObject JSON) throws JResponseError {
                boolean status = super.onResponseReceived(response, JSON);

                if(JSON.isNull("error")){
                    if(listeners!=null){
                        printJson(JSON);
                        listeners.onSuccess(new ADayOutlook(JSON));
                    }
                }
                else{
                    throw new JResponseError(Utils.getMapFromJsonObject(AJSONObject.optJSONObject(JSON, "error")));
                }

                return status;

            }

            @Override
            protected void onError(Exception e) {
                super.onError(e);
                if(listeners!=null){
                    listeners.onFailure(e);
                }
            }

        };

        return ApiServiceUtils.callService(context,service);

    }

    public boolean getHeavyRainWarning(final NowcastListeners listeners){

        JSONObject params = new JSONObject();
        try {
            params.put(DATASET, context.getString(R.string.heavy_rain_warning));
            params.put(KEY_REF, context.getString(R.string.keyref));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(e.getMessage()+" at getTwoHourNowcast Module!!");
        }

        RUServiceBase service = new RUServiceBase(context, R.string.weather_api, params, null,  null, ServerReuqestMethod.GET){

            @Override
            protected boolean onResponseReceived(String response,JSONObject JSON) throws JResponseError {
                boolean status = super.onResponseReceived(response, JSON);

                if(JSON.isNull("error")){
                    if(listeners!=null){
                        printJson(JSON);
                        listeners.onSuccess(new ANowcast(JSON));
                    }
                }
                else{
                    throw new JResponseError(Utils.getMapFromJsonObject(AJSONObject.optJSONObject(JSON, "error")));
                }

                return status;

            }

            @Override
            protected void onError(Exception e) {
                super.onError(e);
                if(listeners!=null){
                    listeners.onFailure(e);
                }
            }

        };

        return ApiServiceUtils.callService(context,service);

    }

    // For testing purpose
    void printJson(JSONObject JSON){

        boolean canPrint = false;

        if (canPrint){

            Log.e(" ");
            Log.e("JSON "+JSON);
            Log.e(" ");
        }

    }
}
