package com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AForecastIssue;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ARelativeHumidity;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ATemperature;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AWind;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AMain extends JsonObj {

    public String title;
    public String validTime;
    public String wxmain;
    public String pastweather;
    public String forecast;
    public AForecastIssue forecastIssue;
    public ATemperature temperature;
    public ARelativeHumidity relativeHumidity;
    public AWind wind;

    public AMain(JSONObject map) {
        super(map);
        if(!isEmpty){

            title 		        = AJSONObject.optString(map, "title");
            validTime 	        = AJSONObject.optString(map, "validTime");
            wxmain 		        = AJSONObject.optString(map, "wxmain");
            pastweather         = AJSONObject.optString(map, "pastweather");
            forecast 	        = AJSONObject.optString(map, "forecast");
            temperature         = new ATemperature(AJSONObject.optJSONObject(map, "temperature"));
            relativeHumidity    = new ARelativeHumidity(AJSONObject.optJSONObject(map, "relativeHumidity"));
            wind                = new AWind(AJSONObject.optJSONObject(map, "wind"));

        }
    }


}