package com.appcel.zuhair.appceltest.adapter.holder;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appcel.zuhair.appceltest.R;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AWeatherForecast;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.ANowcast;
import com.appcel.zuhair.appceltest.common.utility.Log;
import com.appcel.zuhair.appceltest.common.utility.Utils;
import com.appcel.zuhair.appceltest.fragments.AreaListFragment;

/**
 * Created by zuhair on 24/9/16.
 */

public class TwoHourForecastHolder extends ViewHolder{

    ANowcast nowcast;

    TextView temp_txt;
    TextView humid_txt;
    TextView nowcastTitle;

    ImageView nowcast_icon;

    Button areaListBtn;

    Context context;
    Activity act;

    DialogFragment dialogFragment;


    public TwoHourForecastHolder(){

    }

    public TwoHourForecastHolder(Activity act,ANowcast nowcast){
        //this.context = context;
        this.nowcast = nowcast;
        this.act     = act;
    }

    @Override
    public void renderView(View v) {

        temp_txt        = (TextView)v.findViewById(R.id.temp_txt);
        humid_txt       = (TextView)v.findViewById(R.id.humid_txt);
        nowcastTitle    = (TextView)v.findViewById(R.id.title);

        nowcast_icon    = (ImageView) v.findViewById(R.id.nowcast_icon);

        areaListBtn     = (Button) v.findViewById(R.id.area_list_btn);
        initializeListener();
        updateDataInfo();
    }

    void initializeListener(){

        areaListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAreaList();

            }
        });

    }

    @Override
    public int getLayout() {
        return R.layout.two_hour_forecast_cview;
    }

    @Override
    protected void updateDataInfo() {
        nowcastTitle.setText(nowcast.channel.title);
        nowcast_icon.setBackgroundResource(Utils.getIconFromAbbreviation(""));
    }

    void showAreaList(){

        String fragmentTag = "AREA_LIST";

        AWeatherForecast weatherForecast = nowcast.channel.item.weatherForecast;

        dialogFragment = new AreaListFragment(weatherForecast.areaList){

            @Override
            public void onBackPressed() {
                dialogFragment.dismiss();
            }
        };

        try{
            dialogFragment.show(act.getFragmentManager(),fragmentTag);

        } catch (Exception e) {
            Log.d("", "Can't get the fragment manager with this");
        }
    }


}
