package com.appcel.zuhair.appceltest.common.model.responsemodel.outlook;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ARelativeHumidity;
import com.appcel.zuhair.appceltest.common.model.responsemodel.ATemperature;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AWind;
import com.appcel.zuhair.appceltest.common.utility.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zuhair on 24/9/16.
 */

public class AWeatherForecast extends JsonObj {

    public ArrayList<String> dayList;
    public ArrayList<String> forecastList;
    public ArrayList<String> iconList;
    public ArrayList<ATemperature> temperatureList;
    public ArrayList<ARelativeHumidity> relativeHumidityList;
    public ArrayList<AWind> windList;

    public AWeatherForecast(JSONObject map) {
        super(map);
        if(!isEmpty){

            JSONArray dayArr = AJSONObject.optJSONArray(map, "day");
            dayList = new ArrayList<String>();
            if(dayArr!=null){
                for (int i = 0; i < dayArr.length(); i++) {
                    dayList.add(dayArr.optString(i));
                }
            }


            JSONArray forecastArr = AJSONObject.optJSONArray(map, "forecast");
            forecastList    = new ArrayList<String>();
            if(forecastArr!=null){
                for (int i = 0; i < forecastArr.length(); i++) {
                    forecastList.add(forecastArr.optString(i));
                }
            }

            JSONArray iconArr = AJSONObject.optJSONArray(map, "icon");
            iconList    = new ArrayList<String>();
            if(iconArr!=null){
                for (int i = 0; i < iconArr.length(); i++) {
                    iconList.add(iconArr.optString(i));
                }
            }

            JSONArray temperatureArr = AJSONObject.optJSONArray(map, "temperature");
            temperatureList = new ArrayList<ATemperature>();
            if(temperatureArr!=null){
                for (int i = 0; i < temperatureArr.length(); i++) {
                    temperatureList.add(new ATemperature(temperatureArr.optJSONObject(i)));
                }
            }


            JSONArray relativeHumidityArr = AJSONObject.optJSONArray(map, "relativeHumidity");
            relativeHumidityList    = new ArrayList<ARelativeHumidity>();
            if(relativeHumidityArr!=null){
                for (int i = 0; i < relativeHumidityArr.length(); i++) {
                    relativeHumidityList.add(new ARelativeHumidity(relativeHumidityArr.optJSONObject(i)));
                }
            }

            JSONArray windArr = AJSONObject.optJSONArray(map, "wind");
            windList    = new ArrayList<AWind>();
            if(windArr!=null){
                for (int i = 0; i < windArr.length(); i++) {
                    windList.add(new AWind(windArr.optJSONObject(i)));
                }
            }


        }
    }
}
