package com.appcel.zuhair.appceltest.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;


import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.appcel.zuhair.appceltest.R;
import com.appcel.zuhair.appceltest.adapter.AreaListAdapter;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.ANowcast;
import com.appcel.zuhair.appceltest.common.services.WeatherService;
import com.appcel.zuhair.appceltest.common.utility.Log;
import com.appcel.zuhair.appceltest.common.utility.Utils;

/**
 * Created by zuhairahmad on 22/9/16.
 */
public class WeatherFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private RecyclerView.LayoutManager mLayoutManager;

    RecyclerView areaRecyclerView;

    AreaListAdapter adapter;
    ImageButton back;

    Activity activity;

    ANowcast nowcastD;

    private SwipeRefreshLayout swipeRefreshLayout;

    public WeatherFragment(Activity activity){
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.weather_view, container, false);

        if (savedInstanceState == null) {

            areaRecyclerView    = (RecyclerView)rootView.findViewById(R.id.area_recycler_view);
            back                = (ImageButton)rootView.findViewById(R.id.back);

            final LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

            areaRecyclerView.setLayoutManager(layoutManager);

            swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setOnRefreshListener(this);


            callTwoHourNowcastAPI();
        }

        return rootView;
    }



    void initializeAdapter(){

        if (nowcastD != null){
            adapter = new AreaListAdapter(getActivity(),nowcastD.channel.item.weatherForecast.areaList);

            areaRecyclerView.setAdapter(adapter);

            adapter.notifyDataSetChanged();
        }

    }

    void callTwoHourNowcastAPI(){

        //final ProgressDialog progress = Utils.getProgress(activity, getString(R.string.loading_weather), getString(R.string.wait));
        showSwipeRefresh(true);

        WeatherService service = new WeatherService(activity);
        service.getTwoHourNowcast(new WeatherService.NowcastListeners() {
            @Override
            public void onSuccess(ANowcast nowcast) {

                nowcastD = nowcast;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initializeAdapter();
                    }
                });

                showSwipeRefresh(false);

            }

            @Override
            public void onFailure(Exception error) {
                Log.d("onFailure");
                Log.d(error.getMessage());
                showSwipeRefresh(false);
            }

        });

    }

    @Override
    public void onRefresh() {

        Log.d("onRefresh IN");
        callTwoHourNowcastAPI();
    }

    void showSwipeRefresh(final boolean show){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(show);
            }
        });
    }
}
