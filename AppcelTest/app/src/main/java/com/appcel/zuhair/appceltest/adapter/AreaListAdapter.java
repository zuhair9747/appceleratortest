package com.appcel.zuhair.appceltest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.appcel.zuhair.appceltest.R;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.AArea;
import com.appcel.zuhair.appceltest.common.utility.Log;
import com.appcel.zuhair.appceltest.common.utility.Utils;

import java.util.ArrayList;

/**
 * Created by zuhair on 25/9/16.
 */

public class AreaListAdapter extends RecyclerView.Adapter<AreaListAdapter.ViewHolder>{

    static Context context;
    ArrayList<AArea> areaList;

    public AreaListAdapter(Context context,ArrayList<AArea> areaList) {
        this.context	= context;
        this.areaList   = areaList;

    }

    //==========
    @Override
    public int getItemCount() {

        if (areaList == null)
            return 0;

        return areaList.size();
    }

    //==========
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        AArea area = areaList.get(position);

        if (area != null){
            viewHolder.areaName.setText(area.name);
            viewHolder.forestTxt.setText(Utils.getTextFromAbbreviation(area.forecast));
            viewHolder.forecastIcon.setBackgroundResource(Utils.getIconFromAbbreviation(area.forecast));
        }

    }



    //==========
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.area_row, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;

    }

    //==================================


    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView areaName;
        public ImageView forecastIcon;
        public TextView forestTxt;

        public ViewHolder(View itemLayoutView) {

            super(itemLayoutView);

            areaName     	= (TextView) itemLayoutView.findViewById(R.id.area_name);
            forestTxt       = (TextView) itemLayoutView.findViewById(R.id.forest_txt);
            forecastIcon    = (ImageView)itemLayoutView.findViewById(R.id.forecast_icon);
        }

    }


}
