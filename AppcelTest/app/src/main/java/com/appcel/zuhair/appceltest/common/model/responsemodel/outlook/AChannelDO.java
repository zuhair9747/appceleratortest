package com.appcel.zuhair.appceltest.common.model.responsemodel.outlook;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AChannelDO extends JsonObj {

    public String title;
    public String source;

    public AItemDO item;

    public AChannelDO(JSONObject map) {
        super(map);
        if(!isEmpty){

            title 		= AJSONObject.optString(map, "title");
            source 		= AJSONObject.optString(map, "source");

            item 		= new AItemDO(AJSONObject.optJSONObject(map, "item"));


        }
    }


}
