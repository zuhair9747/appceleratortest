package com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AChannelNF extends JsonObj {

    public String title;
    public String source;
    public String description;
    public AItem item;

    public AChannelNF(JSONObject map) {
        super(map);
        if(!isEmpty){

            title 		= AJSONObject.optString(map, "title");
            source 		= AJSONObject.optString(map, "source");
            description = AJSONObject.optString(map, "description");
            item 		= new AItem(AJSONObject.optJSONObject(map, "item"));


        }
    }


}
