package com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AArea extends JsonObj {

    public String forecast;
    public String lat;
    public String lon;
    public String name;

    public AArea(JSONObject map) {
        super(map);
        if(!isEmpty){

            forecast    = AJSONObject.optString(map, "forecast");
            lat 	    = AJSONObject.optString(map, "lat");
            lon         = AJSONObject.optString(map, "lon");
            name        = AJSONObject.optString(map, "name");

        }
    }

}
