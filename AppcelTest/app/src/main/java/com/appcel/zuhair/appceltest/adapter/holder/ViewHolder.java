
//ViewHolder.java


package com.appcel.zuhair.appceltest.adapter.holder;

import android.view.View;

public abstract class ViewHolder {

	public boolean editable;
	public boolean verified;
	
	public ViewHolder() {
		// TODO Auto-generated constructor stub
	}
	
	public abstract int getLayout();
	public abstract void renderView(View v);
	
	protected abstract void updateDataInfo();
	
	public void onDone() {
	}

}
