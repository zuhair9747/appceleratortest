package com.appcel.zuhair.appceltest.common.model.responsemodel;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class ARelativeHumidity extends JsonObj {

    public String high;
    public String low;
    public String unit;


    public ARelativeHumidity(JSONObject map) {
        super(map);
        if(!isEmpty){

            high    = AJSONObject.optString(map, "high");
            low     = AJSONObject.optString(map, "low");
            unit    = AJSONObject.optString(map, "unit");


        }
    }

    public String getUnit(){

        if (unit.equalsIgnoreCase("Percentage")){
            return "%";
        }else if (unit.equalsIgnoreCase("unit name")){ // to add new unit
            return "unit name";
        }else {
            return unit;
        }

    }
}

