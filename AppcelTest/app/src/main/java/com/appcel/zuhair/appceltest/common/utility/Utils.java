package com.appcel.zuhair.appceltest.common.utility;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.appcel.zuhair.appceltest.R;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

public class Utils {

	private static AlertDialog alert;
	private static AlertDialog decisionAlert;
	private static AlertDialog forceAlert;
	public static Locale locale;

	//==================================================================

	public static float elapsedTimeInFloatSec(long startTime) {
		float resultInSec = (System.currentTimeMillis() - startTime) / 1000.0f;
		return resultInSec;
	}

	public static long elapsedTimeInMillis(long startTime) {
		return System.currentTimeMillis() - startTime;
	}

	public static String elapsedTimeInSec(long startTime) {
		float resultInSec = elapsedTimeInFloatSec(startTime);
		return new DecimalFormat("#0.000").format(resultInSec);
	}

	//==================================================================

	public static void setGradient(int Color1, int Color2, Button vu) {
		Shader textShader = new LinearGradient(0, 0, 0, 20, Color1, Color2, TileMode.CLAMP);
		vu.getPaint().setShader(textShader);
	}

	//==================================================================

	public static void log(String tag, String msg) {
		Log.d(tag, msg);
	}

	//==================================================================

	public static void exception(String tag, Exception exp, String msg) {
		Log.d(tag, exp.getMessage() + " at " + msg);
	}

	//======================================================================
	public static void onError(Context ctx, Exception e) {
		showToast(ctx, "Error: " + e.getLocalizedMessage(), Toast.LENGTH_LONG);
		Log.d("Error =====> " + e.getMessage());
	}

	//======================================================================
	public static void showToast(Context ctx, String text, int duration) {
		try {
			Toast.makeText(ctx, text, duration).show();
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At showToast(...) of Utils");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At showToast(...) of Utils");
		}
	}

	//======================================================================
	public static void showToastOnMainThread(final Context ctx, final String text, final int duration) {
		try {
			((Activity) ctx).runOnUiThread(new Runnable() {

				public void run() {
					// TODO Auto-generated method stub
					showToast(ctx, text, duration);
				}

			});
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At showToastOnMainThread(...) of Utils");
		} catch (ClassCastException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At showToastOnMainThread(...) of Utils");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At showToastOnMainThread(...) of Utils");
		}

	}


	//======================================================================

	public static HashMap<String, String> getMapFromJsonObject(JsonObject json) {

		HashMap<String, String> result = new HashMap<String, String>();

		try {
			for (Iterator<Entry<String, JsonElement>> it = json.entrySet().iterator(); it.hasNext(); ) {
				Entry<String, JsonElement> entry = (Entry<String, JsonElement>) it.next();
				//CelUtils.log(Constants.kApiTag, "Key == "+entry.getKey()+" Value == "+entry.getValue().getAsString());
				result.put(entry.getKey(), entry.getValue().getAsString());
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At getMapFromJsonObject(...) of Utils");
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At getMapFromJsonObject(...) of Utils");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At getMapFromJsonObject(...) of Utils");
		}

		return result;

	}

//======================================================================

	public static HashMap<String, String> getMapFromJsonObject(JSONObject json) {

		HashMap<String, String> result = new HashMap<String, String>();
		try {
			for (Iterator<String> it = json.keys(); it.hasNext(); ) {
				String key = it.next();
				result.put(key, json.optString(key));
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At getMapFromJsonObject(...) of Utils");
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At getMapFromJsonObject(...) of Utils");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At getMapFromJsonObject(...) of Utils");
		}

		return result;

	}


	//======================================================================
	public static void hideSoftKeyboard(Activity activity) {

		try {
			InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			if (activity.getCurrentFocus() != null)
				inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At hideSoftKeyboard(...) of Utility");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At hideSoftKeyboard(...) of Utility");
		}

	}

	//======================================================================
	public static void SoftKeyBoard(Context c, boolean state) {
		try {
			if (!state) {
				InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
				if (((Activity) c).getCurrentFocus() != null)
					imm.hideSoftInputFromWindow(((Activity) c).getCurrentFocus().getWindowToken(), 0);
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At SoftKeyBoard(...) of Utility");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At SoftKeyBoard(...) of Utility");
		}
	}
	//======================================================================

	public static boolean isLocationEnabled(Context context) {
		return (gpsLocEnabled(context) || networkLocEnabled(context));
	}

	public static boolean isLocationEnabled(Context context, String provider) {
		if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
			return networkLocEnabled(context);
		} else if (provider.equals(LocationManager.GPS_PROVIDER)) {
			return gpsLocEnabled(context);
		} else {
			return false;
		}
	}

	public static boolean gpsLocEnabled(Context context) {

		try {
			LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
			return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER); // Return a boolean
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At gpsLocEnabled(...) of Utility");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At gpsLocEnabled(...) of Utility");
		}

		return false;

	}

	public static boolean networkLocEnabled(Context context) {

		try {
			LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
			return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER); // Return a boolean
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At networkLocEnabled(...) of Utility");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At networkLocEnabled(...) of Utility");
		}

		return false;

	}

	//======================================================================
	private static void postSoftKeyBoard(Context c, View v, boolean state) {

		try {
			InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (state) {
				//imm.showSoftInput(v, InputMethod.SHOW_FORCED);
				imm.toggleSoftInputFromWindow(v.getApplicationWindowToken(), 0, 0);
				v.requestFocus();
			} else {
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				v.clearFocus();
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At postSoftKeyBoard(...) of Utils");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At postSoftKeyBoard(...) of Utils");
		}

	}

	public static void SoftKeyboard(final Context c, final View v, final boolean state) {

		try {
			v.post(new Runnable() {

				@Override
				public void run() {
					postSoftKeyBoard(c, v, state);
				}
			});
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At SoftKeyboard(...) of Utils");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At SoftKeyboard(...) of Utils");
		}

	}

	//================================================================
	public static void hideKeyboard(Activity activity) {

		try {
			activity.getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At hideKeyboard(...) of Utils");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At hideKeyboard(...) of Utils");
		}

	}


	//================================================================
	public static String decodeToBase64(String str) {
		String retStr = null;
		try {
			retStr = new String(Base64.decode(str, Base64.DEFAULT));


		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At decodeToBase64(...) of Utils");
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + "At decodeToBase64(...) of Utils");
		}

		return (retStr == null) ? "" : retStr;
	}


	//================================================================

	public static String getAppName(Context pContext) {

		String appName = new String();

		try {
			PackageManager lPackageManager = pContext.getPackageManager();
			ApplicationInfo lApplicationInfo = null;
			lApplicationInfo = lPackageManager.getApplicationInfo(pContext.getApplicationInfo().packageName, 0);
			appName = (lApplicationInfo != null ? lPackageManager.getApplicationLabel(lApplicationInfo).toString() : "Unknown");
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At getAppName(...) of Utils");
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At getAppName(...) of Utils");
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage() + " At getAppName(...) of Utils");
		}

		return appName;
	}

	//======================================================================
	public static boolean isNetworkConnected(Context ctx, boolean showAlert) {

		boolean status = false;
		try{
			ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo ni = cm.getActiveNetworkInfo();
			if (ni != null && ni.isConnectedOrConnecting()){
				status = true;
			}
			else{
				if(showAlert)
					Utils.showAlertOnMainThread(ctx, R.string.error, ctx.getResources().getString(R.string.no_internet));
				status = false;
			}
		}
		catch(NullPointerException e){
			e.printStackTrace();
			Log.e(e.getMessage()+"At isNetworkConnected(...) of Utils");
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage()+"At isNetworkConnected(...) of Utils");
		}

		return status;

	}


	//======================================================================
	public static void showAlertOnMainThread(final Context ctx,final int titleRes, final String text){

		try{
			if(ctx!=null){
				((Activity)ctx).runOnUiThread(new Runnable() {

					public void run() {
						// TODO Auto-generated method stub
						alert(ctx, titleRes, text);
					}
				});
			}
		}
		catch(NullPointerException e){
			e.printStackTrace();
			Log.e(e.getMessage()+" At showAlertOnMainThread(...) module of Utils class");
		}
		catch (ClassCastException e) {
			e.printStackTrace();
			Log.e(e.getMessage()+" At showAlertOnMainThread(...) module of Utils class");
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.e(e.getMessage()+" At showAlertOnMainThread(...) module of Utils class");
		}

	}


	public static void alert(Context c, int titleRes, String message){
		alert(c, R.drawable.aa_common_error, titleRes, message, true, null);
	}

	public static void alert(Context c, int iconRes, int titleRes, String message){
		alert(c, iconRes, titleRes, message, true, null);
	}

	public static void alert(Context c, int iconRes, int titleRes, String message, boolean foreground, DialogInterface.OnCancelListener cancelListener){

		if(foreground){

			try{
				if(alert!=null && alert.isShowing()){
					//======Do nothing
				}
				else{

					AlertDialog.Builder alert = new AlertDialog.Builder(c);

					try{
						alert.setIcon(iconRes);
					}
					catch(Resources.NotFoundException e){
						e.printStackTrace();
						Log.i(e.getMessage()+" at alert(...) of Utils");
					}

					try{
						alert.setTitle(titleRes);
					}
					catch(Resources.NotFoundException e){
						e.printStackTrace();
						Log.i(e.getMessage()+" at alert(...) of Utils");
					}

					alert.setMessage(message);
					alert.setOnCancelListener(cancelListener);

					Utils.alert = alert.create();
					Utils.alert.setCanceledOnTouchOutside(true);
					Utils.alert.show();

				}
			}
			catch(NullPointerException e){
				e.printStackTrace();
				Log.e(e.getMessage()+" at alert(...) of Utils");
			}
			catch (Exception e) {
				e.printStackTrace();
				Log.e(e.getMessage()+" at decisionAlert(...) of Utils");
			}

		}

	}

	//================================================================
	public static void alert(Context c, String message){
		alert(c, R.string.error, message);
	}


	//================================================================
	public static void alert(Context c, String message, DialogInterface.OnCancelListener cancelListener){
		alert(c, R.drawable.aa_common_error, R.string.error, message, true, cancelListener);
	}

	public static void alert(Context c, int title, String message, DialogInterface.OnCancelListener cancelListener){
		alert(c, R.drawable.aa_common_error, title, message, true, cancelListener);
	}

	public static ProgressDialog getProgress(Context ctx, String title, String message){
		return ProgressDialog.show(ctx, title,message,true);
	}

	public static ProgressDialog getProgress(Context ctx,String title){
		return getProgress(ctx, title,ctx.getString(R.string.crop__wait));
	}

	public static ProgressDialog getProgress(Context ctx){
		return getProgress(ctx, ctx.getString(R.string.loading), ctx.getString(R.string.crop__wait));
	}

	public static int getIconFromAbbreviation(String abbreviation){

		switch (abbreviation){
			case "BR":
				return R.drawable.br;
			case "CL":
				return R.drawable.cl;
			case "DR":
				return R.drawable.dr;
			case "FA":
				return R.drawable.fa;
			case "FG":
				return R.drawable.fg;
			case "FN":
				return R.drawable.fn;
			case "FW":
				return R.drawable.fw;
			case "HG":
				return R.drawable.hg;
			case "HR":
				return R.drawable.hr;
			case "HS":
				return R.drawable.hs;
			case "HT":
				return R.drawable.ht;
			case "HZ":
				return R.drawable.hz;
			case "LH":
				return R.drawable.lh;
			case "LR":
				return R.drawable.lr;
			case "LS":
				return R.drawable.ls;
			case "OC":
				return R.drawable.oc;
			case "PC":
				return R.drawable.pc;
			case "PN":
				return R.drawable.pn;
			case "PS":
				return R.drawable.ps;
			case "RA":
				return R.drawable.ra;
			case "SH":
				return R.drawable.sh;
			case "SK":
				return R.drawable.sk;
			case "SN":
				return R.drawable.sn;
			case "SR":
				return R.drawable.sr;
			case "SS":
				return R.drawable.ss;
			case "SU":
				return R.drawable.su;
			case "SW":
				return R.drawable.sw;
			case "TL":
				return R.drawable.tl;
			case "WC":
				return R.drawable.wc;
			case "WD":
				return R.drawable.wd;
			case "WF":
				return R.drawable.wf;
			case "WR":
				return R.drawable.wr;
			case "WS":
				return R.drawable.ws;


		}

		return R.drawable.rain;
	}

	public static String getTextFromAbbreviation(String abbreviation){

		switch (abbreviation){
			case "BR":
				return "Mist";
			case "CL":
				return "Cloudy";
			case "DR":
				return "Drizzle";
			case "FA":
				return "Fair (Day)";
			case "FG":
				return "Fog";
			case "FN":
				return "Fair (Night)";
			case "FW":
				return "Fair & Warm";
			case "HG":
				return "Heavy Thundery Showers with Gusty Winds";
			case "HR":
				return "Heavy Rain";
			case "HS":
				return "Heavy Showers";
			case "HT":
				return "Heavy Thundery Showers";
			case "HZ":
				return "Hazy";
			case "LH":
				return "Slightly Hazy";
			case "LR":
				return "Light Rain";
			case "LS":
				return "Light Showers";
			case "OC":
				return "Overcast";
			case "PC":
				return "Partly Cloudy (Day)";
			case "PN":
				return "Partly Cloudy (Night)";
			case "PS":
				return "Passing Showers";
			case "RA":
				return "Moderate Rain";
			case "SH":
				return "Showers";
			case "SK":
				return "Strong Winds, Showers";
			case "SN":
				return "Snow";
			case "SR":
				return "Strong Winds, Rain";
			case "SS":
				return "Snow Showers";
			case "SU":
				return "Sunny";
			case "SW":
				return "Strong Winds";
			case "TL":
				return "Thundery Showers";
			case "WC":
				return "Windy, Cloudy";
			case "WD":
				return "Windy";
			case "WF":
				return "Windy, Fair";
			case "WR":
				return "Windy, Rain";
			case "WS":
				return "Windy, Showers";


		}

		return "N/A";
	}

}