package com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class ANowcast extends JsonObj {


    public AChannelNF channel;

    public ANowcast(JSONObject map) {
        super(map);
        if(!isEmpty){

            channel = new AChannelNF(AJSONObject.optJSONObject(map, "channel"));

        }
    }
}