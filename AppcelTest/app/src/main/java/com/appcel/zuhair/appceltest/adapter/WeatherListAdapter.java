package com.appcel.zuhair.appceltest.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appcel.zuhair.appceltest.adapter.holder.DayForecastHolder;
import com.appcel.zuhair.appceltest.adapter.holder.OutlookHolder;
import com.appcel.zuhair.appceltest.adapter.holder.TwoHourForecastHolder;
import com.appcel.zuhair.appceltest.adapter.holder.ViewHolder;
import com.appcel.zuhair.appceltest.R;

import com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast.AForecast;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.ANowcast;
import com.appcel.zuhair.appceltest.common.model.responsemodel.outlook.ADayOutlook;
import com.appcel.zuhair.appceltest.common.model.responsemodel.outlook.AItemDO;
import com.appcel.zuhair.appceltest.common.model.responsemodel.outlook.AWeatherForecast;
import com.appcel.zuhair.appceltest.common.utility.Log;
import com.appcel.zuhair.appceltest.custom.HeaderListAdapter;

/**
 * Created by zuhair on 24/9/16.
 */

public class WeatherListAdapter extends HeaderListAdapter{

    ANowcast nowcast;
    AForecast forecast;
    ADayOutlook dayOutlook;

    Activity act;

    public WeatherListAdapter(Activity act,ANowcast nowcast,AForecast forecast,ADayOutlook dayOutlook){

        this.act = act;
        this.nowcast = nowcast;
        this.forecast = forecast;
        this.dayOutlook = dayOutlook;

        initializeView();
    }

    @Override
    public View getHeaderView(int pos, View convertView, ViewGroup parent, Object obj,int itemCount) {

        if(convertView == null){

            convertView = LayoutInflater.from(act).inflate(R.layout.weather_list_header_view, parent, false);
        }

        TextView headerText 	= (TextView)convertView.findViewById(R.id.header_text);
        TextView headerValue 	= (TextView)convertView.findViewById(R.id.header_value);
        ImageView headerImage 	= (ImageView)convertView.findViewById(R.id.header_image);

        if(obj instanceof String){

            String textStr = obj.toString();
            headerText.setText(textStr);


        }

        return convertView;
    }

    @Override
    public View getItemView(int pos, View convertView, ViewGroup parent, Object obj) {

        ViewHolder holder = (ViewHolder)obj;
        convertView = LayoutInflater.from(act).inflate(holder.getLayout(), parent, false);
        holder.renderView(convertView);

        return convertView;
    }


    public void setDayOutlook(ADayOutlook dayOutlook) {
        if (this.dayOutlook == null){
            this.dayOutlook = dayOutlook;
            //initializeView();
            //====Add Day Outlook
            if (dayOutlook != null){
                addOutlookInfoCell();
            }
        }
    }

    public void setForecast(AForecast forecast) {
        if (this.forecast == null){
            this.forecast = forecast;
            //initializeView();
            //====Add Day Forecast
            if (forecast != null){
                addItem(new DayForecastHolder(forecast));
            }
        }
    }

    public void setNowcast(ANowcast nowcast) {
        if (this.nowcast == null){
            this.nowcast = nowcast;
            //initializeView();
            //====Add Two Hour Forecast
            if (nowcast != null)
            {
                addItem(new TwoHourForecastHolder(act,nowcast));
            }
        }
    }

    private void initializeView(){

        clear();

        /*//====Add Two Hour Forecast
        if (nowcast != null)
        {
            Log.d("ADDING TwoHourForecastHolder");
            addItem(new TwoHourForecastHolder(act,nowcast));
        }*/

        //====Add Day Forecast
        if (forecast != null){

            Log.d("ADDING DayForecastHolder");
            addItem(new DayForecastHolder(forecast));
        }



        //====Add Day Outlook
        if (dayOutlook != null){

            addHeader(dayOutlook.channel.item.title);
            addOutlookInfoCell();
        }

    }

    private void addOutlookInfoCell(){

        AItemDO item = dayOutlook.channel.item;
        if (item != null){
            AWeatherForecast forecast = item.weatherForecast;
            if (forecast != null){
                for (int i = 0;i< forecast.dayList.size() ;i ++){
                    addItem(new OutlookHolder(forecast.dayList.get(i),forecast.forecastList.get(i),forecast.iconList.get(i),forecast.temperatureList.get(i),forecast.relativeHumidityList.get(i),forecast.windList.get(i)));
                }
            }
        }
    }


}
