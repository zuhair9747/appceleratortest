package com.appcel.zuhair.appceltest.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.appcel.zuhair.appceltest.R;
import com.appcel.zuhair.appceltest.adapter.WeatherListAdapter;
import com.appcel.zuhair.appceltest.common.model.responsemodel.dayforecast.AForecast;
import com.appcel.zuhair.appceltest.common.model.responsemodel.nowcast.ANowcast;
import com.appcel.zuhair.appceltest.common.model.responsemodel.outlook.ADayOutlook;
import com.appcel.zuhair.appceltest.common.services.WeatherService;
import com.appcel.zuhair.appceltest.common.utility.Log;
import com.appcel.zuhair.appceltest.common.utility.Utils;

/**
 * Created by zuhairahmad on 22/9/16.
 */
public class DashboardFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    ADayOutlook dayOutlookD;
    AForecast forecastD;
    ANowcast nowcastD;

    Context context;

    ListView listView;
    WeatherListAdapter adapter;

    Activity act;

    private SwipeRefreshLayout swipeRefreshLayout;

    public DashboardFragment(){

    }

    public DashboardFragment(Activity act){
        this.context    = act;
        this.act        = act;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.dashboard_view, container, false);

        if (savedInstanceState == null) {

            swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setOnRefreshListener(this);

            listView = (ListView)rootView.findViewById(R.id.list_view);


            if (dayOutlookD == null || forecastD == null || nowcastD == null){
                callAPI();
            }

        }

        return rootView;
    }

    void initializeAdapter() {

        adapter = new WeatherListAdapter(getActivity(),nowcastD,forecastD,dayOutlookD);

        listView.setAdapter(adapter);

    }

    void callAPI(){

        callForecastForDayAPI();
        //callTwoHourNowcastAPI();
    }


    void callTwoHourNowcastAPI(){

        showSwipeRefresh(true);

        WeatherService service = new WeatherService(context);
        service.getTwoHourNowcast(new WeatherService.NowcastListeners() {
            @Override
            public void onSuccess(ANowcast nowcast) {

                nowcastD = nowcast;

                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                         callForecastForDayAPI();
                    }
                });

                showSwipeRefresh(false);

            }

            @Override
            public void onFailure(Exception error) {
                Log.d("onFailure");
                Log.d(error.getMessage());

                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callForecastForDayAPI();
                    }
                });

                showSwipeRefresh(false);

            }

        });

    }

    void callForecastForDayAPI(){

        showSwipeRefresh(true);

        WeatherService service = new WeatherService(context);
        service.getForecastForDay(new WeatherService.DayForecastListeners(){

            @Override
            public void onSuccess(AForecast forecast) {

                forecastD = forecast;

                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callFourDayOutlookAPI();

                    }
                });

                showSwipeRefresh(false);

            }

            @Override
            public void onFailure(Exception error) {
                Log.d("onFailure");
                Log.d(error.getMessage());

                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callFourDayOutlookAPI();

                    }
                });
                showSwipeRefresh(false);


            }
        });

    }

    void callFourDayOutlookAPI(){

        showSwipeRefresh(true);

        WeatherService service = new WeatherService(context);
        service.getFourDayOutlook(new WeatherService.OutlookListeners(){
            @Override
            public void onSuccess(ADayOutlook dayOutlook) {

                dayOutlookD = dayOutlook;

                showSwipeRefresh(false);

                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initializeAdapter();

                    }
                });

            }

            @Override
            public void onFailure(Exception error) {
                Log.d("onFailure");
                Log.d(error.getMessage());
                showSwipeRefresh(false);
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initializeAdapter();

                    }
                });

            }


        });

    }


    @Override
    public void onRefresh() {

        Log.d("onRefresh IN");
        callAPI();
    }

    void showSwipeRefresh(final boolean show){
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(show);
            }
        });
    }
}
