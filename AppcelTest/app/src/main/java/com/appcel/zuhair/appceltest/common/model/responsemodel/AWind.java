package com.appcel.zuhair.appceltest.common.model.responsemodel;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AWind extends JsonObj {

    public String direction;
    public String speed;


    public AWind(JSONObject map) {
        super(map);
        if(!isEmpty){

            direction   = AJSONObject.optString(map, "direction");
            speed       = AJSONObject.optString(map, "speed");



        }
    }
}
