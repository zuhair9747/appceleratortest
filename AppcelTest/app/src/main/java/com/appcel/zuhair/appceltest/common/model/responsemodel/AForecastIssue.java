package com.appcel.zuhair.appceltest.common.model.responsemodel;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AForecastIssue extends JsonObj {

    public String date;
    public String time;


    public AForecastIssue(JSONObject map) {
        super(map);
        if(!isEmpty){

            date    = AJSONObject.optString(map, "date");
            time    = AJSONObject.optString(map, "time");


        }
    }
}
