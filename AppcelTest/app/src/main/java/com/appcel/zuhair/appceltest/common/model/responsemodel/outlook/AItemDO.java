package com.appcel.zuhair.appceltest.common.model.responsemodel.outlook;

import com.appcel.zuhair.appceltest.common.crypto.AJSONObject;
import com.appcel.zuhair.appceltest.common.model.JsonObj;
import com.appcel.zuhair.appceltest.common.model.responsemodel.AForecastIssue;

import org.json.JSONObject;

/**
 * Created by zuhairahmad on 21/9/16.
 */
public class AItemDO extends JsonObj {

    public String title;

    public AForecastIssue forecastIssue;
    public AWeatherForecast weatherForecast;

    public AItemDO(JSONObject map) {
        super(map);
        if(!isEmpty){

            title 		    = AJSONObject.optString(map, "title");

            forecastIssue   = new AForecastIssue(AJSONObject.optJSONObject(map, "forecastIssue"));
            weatherForecast = new AWeatherForecast(AJSONObject.optJSONObject(map, "weatherForecast"));


        }
    }

}
